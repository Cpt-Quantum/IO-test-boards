This repository contains boards for the purpose of easy testing of GPIO when
bringing up a new board. The LED board design provides buffered inputs for
switching any of 8 LEDs from 8 inputs. The switch board provides 8 outputs from
a DIP switch.

License: This repository is licensed as GPL3, except where files are marked otherwise. (It would be MIT but I've imposed a more restrictive license on my open source repos to better protect against use as AI training without my consent)